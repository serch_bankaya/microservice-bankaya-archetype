# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This repository is for the bankayan archetype, defines the service's structure and naming class and package conventions.
* Version : 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* In order to use the archetype, clone this repository and checkout develop branch.
* Go to the downloaded project and ejecute the following command:
mvn clean install
* Configuration: In order to make the archetype available in maven, go to rootProject/target (the file microservice-bankaya-archetype-0.0.1-SNAPSHOT.jar should be there) and execute the following command:
mvn install:install-file -Dfile=microservice-bankaya-archetype-0.0.1-SNAPSHOT.jar -DgroupId=mx.com.bankaya -DartifactId=microservice-bankaya-archetype -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar
* Using the achetype: To generate a new project using the archetype execute the following command in your workspace (outside the archetype folder).
Follow the instructions in the console for the creation. 
mvn archetype:generate -DarchetypeGroupId=mx.com.bankaya -DarchetypeArtifactId=microservice-bankaya-archetype -DarchetypeVersion=0.0.1-SNAPSHOT


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

